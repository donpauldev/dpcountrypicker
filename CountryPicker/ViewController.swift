//
//  ViewController.swift
//  CountryPicker
//
//  Created by Don Paul PM on 23/10/17.
//  Copyright © 2017 Knowledgelens. All rights reserved.
//

import UIKit

class ViewController: UIViewController, DPCountryPickerDelegate {

    @IBOutlet weak var countryPicker: DPCountryPicker!
    @IBOutlet weak var countryCode: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = true
        countryPicker.setCountry("IN")
        countryPicker.setLocale("India")
        countryPicker.setCountryByName("India")
    }

    func countryPhoneCodePicker(_ picker: DPCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        self.countryCode.text = "\(countryCode) -\(phoneCode)"
    }
    


}

